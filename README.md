# Deteccion de Objetos con TensorFlow

Repositorio del proyecto de deteccion de objetos de botones florales utilizando la biblioteca TensorFlow, a partir de los videos subidos por ***AMP Tech***.

Links:
- [*Video Parte 1*](https://www.youtube.com/watch?v=SJRP0IRfPj0)
- [*Video Parte 2*](https://www.youtube.com/watch?v=EKe05rMG-Ww) 
- [*Repositorio*](https://github.com/puigalex/deteccion_objetos)
 
## Setup

Pasos a seguir para tener el ambiente configurado y preparado para su uso

- Descargar [*LabelImg*](https://tzutalin.github.io/labelImg/) para etiquetar las imagenes

- Tener instalado [*Anaconda*](https://www.anaconda.com/products/individual#Downloads)

- Levantar entorno virtual con Anaconda(leer *enviroment/ANACONDA.md*)

- Descargar un Modelo de Deteccion de Objetos
	- [*TensorFlow 1*](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf1_detection_zoo.md)

## Uso
- Etiquetar las imagenes y guardarlas en *images/*, agarrar cierto porcentaje y guardarlo en test, y el resto en train(deben estar de a pares .jpg y .xml)

- Dependiendo de las clases etiquetadas editar el .config del modelo descargado y la estructura del directorio
	- num_classes: ***numero de clases etiquetadas aqui***
	- fine_tune_checkpoint: ***path al modelo a entrenar***
	- tf_record_input_reader: ***path al train.record o test.record***
	- label_map_path: ***path al label_map.pbtxt***
	
- En la carpeta *config*, editar el *label_map y labels* de acuerdo a sus clases

- Preparamos algunos archivos:
```
python xml_a_csv.py 

# Create train data:
python csv_a_tf.py --csv_input=data/train_labels.csv  --output_path=data/train.record --images=images/train

# Create test data:
python csv_a_tf.py --csv_input=data/test_labels.csv  --output_path=data/test.record --images=images/test
```

- Entrenamos el modelo:
```
python object_detection/train.py --logtostderr --train_dir=train_info --pipeline_config_path=model/tf1/faster_rcnn_resnet101_coco.config
```

- Frizar el modelo:
```
python object_detection/export_inference_graph.py --input_type image_tensor --pipeline_config_path model/tf1/faster_rcnn_resnet101_coco.config  --trained_checkpoint_prefix train/model.ckpt-NRO-DE-PASO --output_directory freeze_model
```

- Realizar prediccion:
```
python object_detection/object_detection_runner.py --input images/pruebas --output images/output --minscore 0.8
```