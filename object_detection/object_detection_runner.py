import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow.compat.v1 as tf
import zipfile
import json
import time
import glob
from datetime import date
from io import StringIO
from PIL import Image
import matplotlib.pyplot as plt
from utils import visualization_utils as vis_util
from utils import label_map_util
from multiprocessing.dummy import Pool as ThreadPool
import argparse, sys

parser = argparse.ArgumentParser()

parser.add_argument('--labels', help='Directorio a label_map.pbtxt', default ='config/label_map.pbtxt')
parser.add_argument('--input', help='Directorio de las imagenes a procesar', default = 'images/pruebas')
parser.add_argument('--output', help='Directorio de las imagenes a procesar', default = 'images/output')
parser.add_argument('--model', help='Directorio al modelo congelado', default = 'freeze_model')
parser.add_argument('--minscore', help='Score minimo para deteccion', default = '0.4')
args = parser.parse_args()

MAX_NUMBER_OF_BOXES = 100
MINIMUM_CONFIDENCE = float(args.minscore)

PATH_TO_LABELS = args.labels
PATH_TO_TEST_IMAGES_DIR = args.input

PATH_TO_OUTPUT = args.output

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=sys.maxsize, use_display_name=True)
CATEGORY_INDEX = label_map_util.create_category_index(categories)

# Path to frozen detection graph. This is the actual model that is used for the object detection.
MODEL_NAME = args.model
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'

global GLOBAL_COUNT
GLOBAL_COUNT = 0

def load_image_into_numpy_array(image):
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape(
        (im_height, im_width, 3)).astype(np.uint8)

## Contar cantidad de objetos detectados
def count_objects(scores, minScore):
    global GLOBAL_COUNT
    count = 0
    for i in range(0, len(scores)):
        if scores is None or scores[i] > minScore:
            count = count + 1

    GLOBAL_COUNT += count
    print(f'\n\tNUERO DE OBJETOS {count}')
    return count

## Guardar en archivo la cantidad de objetos
def save_count(count, imageName):
    f = open(f"{PATH_TO_OUTPUT}/count-{date.today()}.txt", "a")
    f.write(f'{imageName}->{count}\n')
    f.close()

def detect_objects(image_path):
    image = Image.open(image_path)
    image_np = load_image_into_numpy_array(image)
    image_np_expanded = np.expand_dims(image_np, axis=0)

    (boxes, scores, classes, num) = sess.run([detection_boxes, detection_scores, detection_classes, num_detections], feed_dict={image_tensor: image_np_expanded})

    image_np_with_detections = image_np.copy()

    vis_util.visualize_boxes_and_labels_on_image_array(
        image_np_with_detections,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        CATEGORY_INDEX,
        min_score_thresh=MINIMUM_CONFIDENCE,
        use_normalized_coordinates=True,
        line_thickness=8)
    fig = plt.figure()
    fig.set_size_inches(16, 9)

    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)

    # contar objetos y guardarlos en txt
    count = count_objects(np.squeeze(scores), MINIMUM_CONFIDENCE)
    head, tail = os.path.split(image_path)
    img_name = tail.split('.')[0]
    save_count(count, img_name)

    plt.imshow(image_np_with_detections, aspect = 'auto')
    plt.savefig(f'{PATH_TO_OUTPUT}/{img_name}-{date.today()}', dpi = 62)
    plt.close(fig)

# TEST_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_IMAGES_DIR, 'image-{}.jpg'.format(i)) for i in range(1, 4) ]
TEST_IMAGE_PATHS = glob.glob(os.path.join(PATH_TO_TEST_IMAGES_DIR, '*.*'))
print(f'\n\tIMAGES {TEST_IMAGE_PATHS}')

# Load model into memory
print('Loading model...')
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

print('detecting...')
with detection_graph.as_default():
    with tf.Session(graph=detection_graph) as sess:
        image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
        detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
        detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
        detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
        num_detections = detection_graph.get_tensor_by_name('num_detections:0')


        f = open(f"{PATH_TO_OUTPUT}/count-{date.today()}.txt", "w") # si existe el archivo lo vacia
        f.write(f'{len(TEST_IMAGE_PATHS)}\n')
        f.close()
        for image_path in TEST_IMAGE_PATHS:
            print(f'\n\tIMAGE {image_path}')
            detect_objects(image_path)

        save_count(GLOBAL_COUNT, "TOTAL")
        save_count(MINIMUM_CONFIDENCE, "PROBABILIDAD")
